(defproject vise890/zookareg "2.4.1-1"
  :description "Embedded ZOokeeper KAfka and Confluent's Schema REGistry"
  :url "http://gitlab.com/vise890/zookareg"
  :license {:name "Eclipse Public License"
            :url  "http://www.eclipse.org/legal/epl-v10.html"}

  :repositories {"confluent" "https://packages.confluent.io/maven"}

  :dependencies [[org.clojure/clojure "1.10.1"]
                 [integrant/integrant "0.8.0"]
                 [vise890/systema "2019.03.08"]

                 [org.clojure/tools.logging "1.1.0"]
                 [org.clojure/tools.namespace "1.0.0"]
                 [me.raynes/fs "1.4.6"]
                 [org.apache.kafka/kafka_2.12 "2.4.1" :exclusions [org.slf4j/slf4j-log4j12 org.slf4j/slf4j-api]]
                 [org.apache.curator/curator-test "4.3.0"]
                 [io.confluent/kafka-schema-registry "5.4.1"
                  :exclusions [org.apache.kafka/kafka-clients
                               org.apache.kafka/kafka_2.11
                               org.slf4j/slf4j-log4j12
                               org.apache.commons/commons-compress
                               com.thoughtworks.paranamer/paranamer
                               org.slf4j/slf4j-log4j12
                               org.slf4j/slf4j-api]]
                 ]

  :exclusions [[org.slf4j/slf4j-log4j12]]

  :plugins [[lein-codox "0.10.6"]
            [lein-nvd "0.6.0"]]

  :resource-paths ["resources"]

  :profiles {:dev  {:source-paths   ["dev/clj"]
                    :resource-paths ["dev/resources"]
                    :dependencies   [[org.slf4j/jcl-over-slf4j "1.7.30"]
                                     [org.slf4j/jul-to-slf4j "1.7.30"]
                                     [org.slf4j/log4j-over-slf4j "1.7.30"]
                                     [org.slf4j/slf4j-api "1.7.30"]
                                     [ch.qos.logback/logback-classic "1.2.3"]
                                     [ch.qos.logback/logback-core "1.2.3"]

                                     [vise890/scribe "2018.12.01" :exclusions [org.apache.avro/avro]]

                                     [org.apache.kafka/kafka-clients "2.4.1" :exclusions [org.slf4j/slf4j-log4j12 org.slf4j/slf4j-api]]
                                     [ovotech/kafka-avro-confluent "2.1.0-5"
                                      :exclusions [org.slf4j/slf4j-log4j12 org.apache.commons/commons-compress]]
                                     ]}
             :test {:global-vars {*warn-on-reflection* true}}
             :ci   {:deploy-repositories [["clojars" {:url           "https://clojars.org/repo"
                                                      :username      :env
                                                      :password      :env
                                                      :sign-releases false}]]}})
