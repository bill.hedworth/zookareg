# zookareg [![pipeline status](https://gitlab.com/vise890/zookareg/badges/master/pipeline.svg)](https://gitlab.com/vise890/zookareg/commits/master)

Embedded **Zo**okeeper **Ka**fka and Confluent's Schema **Reg**istry.

## [API Docs](https://vise890.gitlab.io/zookareg/)

## Usage

[![Clojars Project](https://img.shields.io/clojars/v/vise890/zookareg.svg)](https://clojars.org/vise890/zookareg)
```clojure
;; in project.clj
[vise890/zookareg "2.4.1-1"]
```

### TL;DR

```clojure
(require 'zookareg.core)

(with-zookareg-fn
  (fn []
     ;; your work here
     ,,,))
```

### Development:

```clojure
(require 'zookareg.core)

;; Start an embedded system with default ports:
;; zookeeper: 2181; kafka: 9092; schema registry: 8081
(init-zookareg)

;; another call will halt the previous system:
(init-zookareg)

;; When you're done:
(halt-zookareg!)
```

### `clojure.test`:

**NOTE**: these will halt running zookareg instances

```clojure
(require 'clojure.test)
(require 'zookareg.core)

(use-fixtures :once with-zookareg-fn)
```

### Other Goodies

```clojure
;; Specify ports & kafka-overrides:
(init-zookareg (assoc-in (read-default-config)
                         [:zookareg.kafka/server :config :log-dir]
                         "/tmp/kafka-log"))
```

Happy testing!

### Versions

Zookareg versions use this format:

```bash
${kafka_version}-${build_number}
```

For example:

```ruby
0.10.0-4 # Kafka v = 0.10.0, Zookareg build = 4
1.0.1-1  # Kafka v = 1.0.1, Zookareg build = 1
```

## [Default Config](./resources/config.edn)

## License

Copyright © 2017 Martino Visintin & Contributors

Distributed under the Eclipse Public License either version 1.0 or (at
your option) any later version.
